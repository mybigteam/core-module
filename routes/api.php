<?php

Route::group([
    'namespace' => 'MyBigTeam\Core\Http\Controllers',
], function () {
    Route::get('/ping', 'PingController@ping');
});