<?php

namespace MyBigTeam\Core\Tests;

use Illuminate\Foundation\Testing\TestResponse;
use Illuminate\Support\Debug\Dumper;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Request as SymfonyRequest;

class TestCase extends \Orchestra\Testbench\TestCase
{
    public $throwHttpExceptions = true;

    public function setUp()
    {
        parent::setUp();

        $this->app['config']->set('app.fallback_locale', 'pt-br');
        $this->app['config']->set('app.locale', 'pt-br');
        
        $this->app->setLocale('pt-br');
        $this->app['translator']->setFallback('pt-br');

        $this->artisan('migrate', [
            '--database' => 'testbench',
        ]);
    }

    /**
     * @return void
     */
    protected function getPackageProviders($app)
    {
        return ['MyBigTeam\Core\Providers\ModuleServiceProvider'];
    }

    /**
     * Define environment setup.
     *
     * @param  \Illuminate\Foundation\Application  $app
     * @return void
     */
    protected function getEnvironmentSetUp($app)
    {
        // Setup default database to use sqlite :memory:
        $app['config']->set('database.default', 'testbench');
        $app['config']->set('database.connections.testbench', [
            'driver'   => 'sqlite',
            'database' => ':memory:',
            'prefix'   => '',
        ]);
    }

    /**
     * Create the test response instance from the given response.
     *
     * @param  \Illuminate\Http\Response  $response
     * @return \Illuminate\Foundation\Testing\TestResponse
     */
    protected function createTestResponse($response)
    {
        if(isset($response->exception) && $response->exception) {
            if($this->throwHttpExceptions) {
                (new Dumper)->dump($response->getContent());
                throw $response->exception;
            }
        }

        return TestResponse::fromBaseResponse($response);
    }

    /**
     * @return Request
     */
    public function makeFakeRequest($method, $uri, $parameters = [], $cookies = [], $files = [], $server = [], $content = null)
    {
        $symfonyRequest = SymfonyRequest::create(
            $this->prepareUrlForRequest($uri), $method, $parameters,
            $cookies, $files, array_replace($this->serverVariables, $server), $content
        );

        return Request::createFromBase($symfonyRequest);
    }
}