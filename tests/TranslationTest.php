<?php

use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Validation\ValidationException;
use Illuminate\Http\Request;

class TranslationTest extends MyBigTeam\Core\Tests\TestCase
{
    use ValidatesRequests;

    public function testTranslateWithValidator()
    {
        $request = $this->makeFakeRequest('GET', '/test', [
            'data' => [
                'attributes' => []
            ]
        ]);

        $e = null;

        try {
            $this->validate($request, [
                'data.attributes.test-date' => 'date|required',
            ]);
        } catch(ValidationException $e) {

        }

        $this->assertNotEmpty($e);

        $validator = $e->validator;

        $message = $validator->errors()->first();

        $this->assertContains('campo data de teste é obrigatório.', $message);
    }
}