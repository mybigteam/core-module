<?php

use Carbon\Carbon;

class PingTest extends MyBigTeam\Core\Tests\TestCase
{
    public function testDoPingRequest()
    {
        $date = Carbon::parse('2000-02-01 10:11:12');

        Carbon::setTestNow($date);

        $this
            ->json('GET', 'api/v1/ping', [], ['Origin' => 'http://teste'])
            ->assertStatus(200)
            ->assertJson([
                'data' => [
                    'id' => 1,
                    'type' => 'ping-response',
                    'attributes' => [
                        'date-time' => $date->format('Y-m-d H:i:s')
                    ]
                ],
            ])
            ->assertHeader('access-control-allow-origin', '*');

        $this
            ->json('OPTIONS', 'api/v1/ping')
            ->assertStatus(200)
            ->assertHeader('access-control-allow-origin', '*');
    }
}