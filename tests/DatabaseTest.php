<?php

use Carbon\Carbon;

use MyBigTeam\Core\Models\User;

class DatabaseTest extends MyBigTeam\Core\Tests\TestCase
{
    public function testSaveAndGetUser()
    {
        $userOne = factory(User::class)->create();

        $this->assertNotEmpty($userOne->id);
        $this->assertFalse(is_numeric($userOne->id));

        $userOne = $userOne->fresh();
        $this->assertNotEmpty($userOne);
        $this->assertFalse(is_numeric($userOne->id));
        $this->assertFalse(is_numeric($userOne->fresh()->id));

        $this->assertNotEmpty($userOne);

        $userTwo = User::find($userOne->id);

        $this->assertNotEmpty($userTwo);


    }
}