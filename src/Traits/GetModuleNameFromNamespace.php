<?php

namespace MyBigTeam\Core\Traits;

trait GetModuleNameFromNamespace
{
    /**
     * @return string
     */
    public function getModuleName()
    {
        $namespace = get_class($this);
        $withoutPrefix = substr($namespace, 10);
        $name = substr($withoutPrefix, 0, strpos($withoutPrefix, '\\'));
        return strtolower($name);
    }
}