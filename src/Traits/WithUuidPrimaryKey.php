<?php

namespace MyBigTeam\Core\Traits;

use Webpatser\Uuid\Uuid;

trait WithUuidPrimaryKey
{
    /**
     * Boot function from laravel.
     */
    protected static function bootWithUuidPrimaryKey()
    {
        static::creating(function ($model) {
            if(!$model->{$model->getKeyName()}) {
                $model->{$model->getKeyName()} = (string) Uuid::generate();
            }
        });
    }

    /**
     * Get the value indicating whether the IDs are incrementing.
     *
     * @return bool
     */
    public function getIncrementing()
    {
        if($this->incrementing) $this->incrementing = false;

        return $this->incrementing;
    }
}