<?php

namespace MyBigTeam\Core\Traits;

use DateTime;
use Cache;

trait TransformAttributes
{
    /**
     * @return array
     */
    public function transformAttributes(array $attributes)
    {
        return array_combine(
            $this->convertKeys(array_keys($attributes)),
            $this->convertValues(array_values($attributes))
        );
    }

    public function convertKeys(array $keys)
    {
        return array_map([$this, 'convertKeyWithArrayCache'], $keys);
    }

    public function convertKeyWithArrayCache($key) 
    {
        $cacheKey = "core/transform-attributes/key-{$key}";

        return Cache::store('array')->rememberForever($cacheKey, function() use($key) {
            return $this->convertKey($key);
        });
    }

    public function convertKey($key)
    {
        if(strpos($key, '_') !== false) {
            return str_replace('_', '-', $key);
        }

        return kebab_case($key);
    }

    public function convertValues(array $values)
    {
        return array_map([$this, 'convertValue'], $values);
    }

    public function convertValue($value)
    {
        if($value instanceof DateTime) {
            return $value->format('Y-m-d H:i:s');
        }
        return $value;
    }
}