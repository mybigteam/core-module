<?php

namespace MyBigTeam\Core\Validation;

use Illuminate\Validation\Validator as BaseValidator;

class Validator extends BaseValidator
{
    /**
     * Get the given attribute from the attribute translations.
     *
     * @param  string  $name
     * @return string
     */
    protected function getAttributeFromTranslations($name)
    {
        $key = "validation.attributes.$name";
        $result = $this->translator->trans($key);

        if($result == $key) {
            if(strpos($name, '.') > 0) {
                $aux = explode('.', $name);
                $result = end($aux);
            } else {
                $result = $name;
            }
        }

        return $result;
    }
}
