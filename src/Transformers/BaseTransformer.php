<?php

namespace MyBigTeam\Core\Transformers;

use League\Fractal;
use MyBigTeam\Core\Traits\TransformAttributes;

abstract class BaseTransformer extends Fractal\TransformerAbstract
{
    use TransformAttributes;

    public function getResourceType()
    {
        $shortName = with(new \ReflectionClass($this))
            ->getShortName();

        return kebab_case(str_replace('Transformer', '', $shortName));
    }
}