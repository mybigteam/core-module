<?php

namespace MyBigTeam\Core\Transformers;

use MyBigTeam\Core\Entities\PingResponse;

class PingResponseTransformer extends BaseTransformer
{
    /**
     * @return array
     */
    public function transform(PingResponse $pingResponse)
    {
        return $this
            ->transformAttributes(get_object_vars($pingResponse));
    }
}