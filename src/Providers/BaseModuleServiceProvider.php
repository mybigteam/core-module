<?php

namespace MyBigTeam\Core\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory as ModelFactory;
use Barryvdh\Cors\HandleCors;

abstract class BaseModuleServiceProvider extends ServiceProvider
{
    public function register() { }

    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot()
    {
        $configName = $this->getModuleName().'-module';

        if(file_exists($configFilePath = $this->path("config/{$configName}.php"))) {
            $this->mergeConfigFrom($configFilePath, $configName);
        }

        if(file_exists($migrationFilePath = $this->path('database/migrations'))) {
            $this->loadMigrationsFrom($migrationFilePath);
        }

        \Route::group([
            'middleware' => ['api', HandleCors::class],
            'prefix' => config('core-module.route-prefix'),
        ], function () {
            if(file_exists($routeFilePath = $this->path('routes/api.php'))) {
                $this->loadRoutesFrom($routeFilePath);
            }
        });

        if($this->app->environment('testing')) {
            if(file_exists($factoriesFilePath = $this->path('database/factories'))) {
                $this->app->make(ModelFactory::class)->load($factoriesFilePath);
            }
        }

        if(file_exists($langFilePath = $this->path('resources/lang'))) {
            $this->loadTranslationsFrom($langFilePath, $this->getModuleName());
        }
    }

    /**
     * @return string
     */
    abstract function getModuleName();

    /**
     * @param string $name
     *
     * @return string
     */
    public function path($name)
    {
        return realpath($this->getBaseDirectory() . '/' . $name);
    }

    /**
     * @return string
     */
    abstract function getBaseDirectory();
}