<?php

namespace MyBigTeam\Core\Providers;

use MyBigTeam\Core\Traits\GetModuleNameFromNamespace;
use Illuminate\Contracts\Debug\ExceptionHandler;
use MyBigTeam\Core\Exceptions\Handler;
use MyBigTeam\Core\Providers\TranslationServiceProvider;
use MyBigTeam\Core\Validation\Validator;
use Illuminate\Contracts\Translation\Translator;

class ModuleServiceProvider extends BaseModuleServiceProvider
{
    use GetModuleNameFromNamespace;

    public function register()
    {
        parent::register();

        $this->app->register(\Barryvdh\Cors\ServiceProvider::class);
    }

    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot()
    {
        $this->bootTranslation();
        $this->bootValidation();

        parent::boot();

        $this->mergeConfigFrom($this->path("config/laravel-fractal.php"), 'laravel-fractal');
        $this->mergeConfigFrom($this->path("config/cors.php"), 'cors');
        $this->app->singleton(ExceptionHandler::class, Handler::class);
    }

    public function bootTranslation()
    {
        $this->app['translator'];
        $this->app->register(TranslationServiceProvider::class);
    }

    public function bootValidation()
    {
        $this->app['validator']->resolver(function() {
            return call_user_func_array(
                [$this, 'resolveValidator'],
                func_get_args()
            );
        });
    }

    /**
     * Resolve a new Validator instance.
     *
     * @return \Illuminate\Validation\Validator
     */
    protected function resolveValidator(Translator $translator, array $data, array $rules, array $messages, array $customAttributes)
    {
        return new Validator($translator, $data, $rules, $messages, $customAttributes);
    }

    /**
     * @return string
     */
    public function getBaseDirectory()
    {
        return realpath(__DIR__.'/../..');
    }
}