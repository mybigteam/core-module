<?php

namespace MyBigTeam\Core\Http\Controllers;

use MyBigTeam\Core\Entities\PingResponse;
use MyBigTeam\Core\Transformers\PingResponseTransformer;
use Carbon\Carbon;

class PingController extends BaseController
{
    public function ping()
    {
        $pingResponse = new PingResponse;
        $pingResponse->id = 1;
        $pingResponse->dateTime = Carbon::now();

        return $this->response(
            $pingResponse,
            resolve(PingResponseTransformer::class)
        );
    }
}