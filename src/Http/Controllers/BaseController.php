<?php

namespace MyBigTeam\Core\Http\Controllers;

use Illuminate\Support\Collection;
use MyBigTeam\Core\Transformers\BaseTransformer;
use Illuminate\Support\Traits\Macroable;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Http\JsonResponse;

abstract class BaseController
{
    use Macroable, ValidatesRequests, AuthorizesRequests;

    /**
     * @return JsonResponse
     */
    protected function response($data, BaseTransformer $transformer)
    {
        $fractal = fractal();

        if(is_array($data) || $data instanceof Collection) {
            $fractal->collection($data);
        } else {
            $fractal->item($data);
        }

        $array = $fractal
            ->transformWith($transformer)
            ->withResourceName($transformer->getResourceType())
            ->toArray();

        return response()->json($array);
    }
}