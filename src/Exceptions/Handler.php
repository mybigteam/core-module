<?php

namespace MyBigTeam\Core\Exceptions;

use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Support\MessageBag;

class Handler extends ExceptionHandler
{
    public function render($request, Exception $e)
    {
        return response()->json(
            $this->getResponseData($e),
            $this->getStatusCode($e)
        );
    }

    /**
     * @return array
     */
    public function getResponseData(Exception $e)
    {
        $errors = [];

        if($e instanceof ValidationException) {
            $errors = $this->getValidationErrors($e->validator->errors());
        } else {
            $errors[] = [
                'code' => $this->getErrorCode($e),
                'detail' => $this->getErrorDetail($e)
            ];
        }

        return [
            'errors' => $errors
        ];
    }

    /**
     * @return array
     */
    public function getValidationErrors(MessageBag $errors)
    {
        $messages = array_map(function($message) {
            return [
                'code' => 'validation',
                'detail' => reset($message),
            ];
        }, $errors->getMessages());

        $errors = [];

        foreach($messages as $attributeName => $error) {
            $errors[] = $error + [
                'source' => [
                    'pointer' => '/'.str_replace('.', '/', $attributeName)
                ]
            ];
        }

        return $errors;
    }

    /**
     * @return string
     */
    public function getErrorCode(Exception $e)
    {
        $shortName = with(new \ReflectionClass($e))
            ->getShortName();

        return kebab_case(str_replace(['Exception', 'Http'], '', $shortName));
    }

    /**
     * @return string
     */
    public function getErrorDetail(Exception $e)
    {
        if($e instanceof HttpExceptionInterface) {
            if($e->getMessage()) {
                return $e->getMessage();
            }

            if(array_key_exists($e->getStatusCode(), Response::$statusTexts)) {
                return Response::$statusTexts[$e->getStatusCode()];
            }
        }

        if($e instanceof AuthenticationException)
            return $e->getMessage();

        return 'an internal error occurred';
    }

    /**
     * @return int
     */
    public function getStatusCode(Exception $e)
    {
        if(method_exists($e, 'getStatusCode'))
            return $e->getStatusCode();

        if($e instanceof ValidationException)
            return 422;

        if($e instanceof AuthenticationException)
            return 401;

        return 500;
    }
}