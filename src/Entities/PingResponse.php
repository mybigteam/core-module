<?php

namespace MyBigTeam\Core\Entities;

use Carbon\Carbon;

class PingResponse
{
    /**
     * @var string
     */
    public $id;

    /**
     * @var Carbon
     */
    public $dateTime;
}