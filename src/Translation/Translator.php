<?php

namespace MyBigTeam\Core\Translation;

use Illuminate\Translation\Translator as BaseTranslator;

class Translator extends BaseTranslator
{
    /**
     * @return string|array|null
     */
    protected function getLine($namespace, $group, $locale, $item, array $replace)
    {
        $line = null;

        $namespaces = $this->getNamespaces($namespace, $group);

        while(($currentNamespace = array_shift($namespaces)) && !$line) {
            $line = $this->getLineWithPrefix($currentNamespace, $group, $locale, $item, $replace);
        }

        return $line;
    }

    /**
     * @return array
     */
    public function getNamespaces($namespace, $group)
    {
        $namespaces = [$namespace];

        if($namespace != 'core') {
            $namespaces[] = 'core';
        }

        if($namespace != '*') {
            $namespaces[] = '*';
        }

        if($group == 'validation') {
            foreach($this->getLoader()->namespaces() as $namespaceName => $namespacePath) {
                if(!in_array($namespaceName, $namespaces)) {
                    $namespaces[] = $namespaceName;
                }
            }
        }

        return $namespaces;
    }

    /**
     * @return string|array|null
     */
    protected function getLineWithPrefix($namespace, $group, $locale, $item, array $replace)
    {
        $line = $this->getLineWithKebabCase($namespace, $group, $locale, $item, $replace);

        if(!$line && strpos($item, 'data.attributes.') !== false) {
            $item = str_replace('data.attributes.', '', $item);
            $line = $this->getLineWithKebabCase($namespace, $group, $locale, $item, $replace);
        }

        if(!$line && strpos($item, 'filter.') !== false) {
            $item = str_replace('filter.', '', $item);
            $line = $this->getLineWithKebabCase($namespace, $group, $locale, $item, $replace);
        }

        return $line;
    }

    /**
     * @return string|array|null
     */
    protected function getLineWithKebabCase($namespace, $group, $locale, $item, array $replace)
    {
        $line = $this->getLineSimple($namespace, $group, $locale, $item, $replace);

        if(!$line && strpos($item, '-') !== false) {
            $item = str_replace('-', '_', $item);
            $line = $this->getLineSimple($namespace, $group, $locale, $item, $replace);
        }

        return $line;
    }

    /**
     * @return string|array|null
     */
    public function getLineSimple($namespace, $group, $locale, $item, array $replace)
    {
        return parent::getLine($namespace, $group, $locale, $item, $replace);
    }
}